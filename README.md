# AMiCli2InfluxDB: Application Monitoring i Client to InfluxDB

[![built with Groovy2.8](https://img.shields.io/badge/built%20with-Groovy-red.svg)](http://www.groovy-lang.org)
[![built with Java8](https://img.shields.io/badge/built%20with-Java-green.svg)](https://www.java.com/en/)

> **DISCLAIMER**
    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


### Version: 0.1 - release 01/07/2018 - Viseth Chaing

Current supported environment: Windows / Linux  
Tested with java 8, InfluxDB 1.5.3

> **Pre-requisite** 
- IndluxDB server is up and running
- java jre 1.8+

> **Deployment** 
- Clone this repo. 
- Unzip and navigate to the content dir. 
- Modify according to your environment the default config.cfg file. 
- According to your environment move the chromedriver and phantomjs binaries from the OS folder to below folder.
=> You're now ready to start creating you first monitoring AMi scenario

> **Create your first scenario** 
- Create a folder inside "AMiCli2InfluxDB\data\AMi" folder. This folder name will be your web base scenario name.
- Create your monitor config file. This config file contains the Selenuim steps (language used is either Groovy or Java).
- Upon running the monitor check, 2 folders will be created inside this folder: the 'logs' folder that contains the log activity of this monitor; and the 'ScreenShots' folder which contains the screenShotOnError pictures. In other words, these are screenshots taken at the failure step of the monitor check.

```
mmi.URLSteps='''
//driver.manage().window().maximize()
driver.get("http://www.indeed.co.uk")
//Thread.sleep(65000)
Thread.sleep(1000)

if(driver.findElement(By.id("text-input-what"))) {
	driver.findElement(By.id("text-input-what")).sendKeys(MMiDecrypt('BJ7DZlsbzN57rRA/OOWQYQ=='))
}
if(driver.findElement(By.id("text-input-where"))) {
	driver.findElement(By.id("text-input-where")).clear()
}
if(driver.findElement(By.id("text-input-where"))) {
	driver.findElement(By.id("text-input-where")).sendKeys("London")
}
Thread.sleep(1000)
if(driver.findElement(By.xpath("//button[@type='submit']"))) {
	driver.findElement(By.xpath("//button[@type='submit']")).click()
}

if(driver.getPageSource().contains("Test")) {
	log.info "PatternSearch String found: ${new Date()}"
}

captureScreenshot(driver,"indeedResult")

driver.getPageSource().contains("Test")
'''
```


### Multi-threaded and asynchronous execution of the scenarii with a headless browser:
AMi2InfluxDB.jar program will scan the "/data/AMi" folder and go through each individual scenario folder and play it. These scenarii will be play asynchronously. 
The program is multi-threaded meaning (with a default of 10 threads) that a new thread will be spawned to handle a given scenario.
The global config file 'config.cfg' contains the connection details to InfluxDB along with the datasource info settings to store AMi Cli data.

```bash
java -jar -XX:-HeapDumpOnOutOfMemoryError -XX:-CreateMinidumpOnCrash AMi2InfluxDB.jar

or with java options

java -jar -Xms1G -Xmx1G -XX:+HeapDumpOnOutOfMemoryError -XX:-CreateMinidumpOnCrash -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:gc.log AMi2InfluxDB.jar
```


### Checking a single scenario with a live browser Chrome (pre-requisite Chrome is installed):
The config file 'config_<ScenarioName>.cfg' contains the monitor check steps.

```bash
cd <AMiCli2InfluxDB_root_dir>
java -jar -DlogDir="./data/AMi/<ScenarioName>" -DurlTitle="<ScenarioName>" -DconfigFile="./data/AMi/<ScenarioName>/config_<ScenarioName>.cfg" -Dshow=true ./exec/AMiCli2InfluxDB.jar
```

The execution will launch a web browser and the scenario steps will start the user simulation navigation.  


### InfluxDB user password encryption:
To encrypt a password used in the Selenuim scenario copy/paste the output of the below command

```bash
java -jar -DpwdEncrypt=<myNewPwd> exec/AMiCli2InfluxDB.jar
``` 

Then use the encrypted string in the MMiDecrypt fonction inside your Selenuim script
```bash
MMiDecrypt('BJ7DZlsbzN57rRA/OOWQYQ==')
``` 


### Note:
You can either create your own Selenium scenarii or use existing scenarii.

Your Selenuim scenarii can be very simple or very complexe.

Whenever a scenario fails, a screenshot at the failed step will be taken and accessible at any given time later.



**Logfiles dir**

- General Logfiles: 

```bash
        - LogLevel: INFO, WARN, ERROR, DEBUG
        - LogSize: 20MB
        - LogRotate: 5 files
        - Directory of the logfile: 'logs/' 
        - Name: 'AMi2InfluxDB.log'

To turn debugging mode so that debugging lines will appear in the logfile:
java -Dapp.env=DEBUG -XX:+HeapDumpOnOutOfMemoryError -XX:-CreateMinidumpOnCrash AMi2InfluxDB.jar

```

- Specific scenarii Logfiles:  

```bash
        - LogLevel: INFO, WARN, ERROR, DEBUG
        - LogSize: 20MB
        - LogRotate: 5 files
        - Directory of the logfile: 'data/AMi/<title>/logs/' 
        - Name: 'AMiCli2InfluxDB.log'

```


**Screenshot on error**
- the last screenshot on error is named 'screenShotOnError.png'
other screenshots (default 10 max) are saved in the below dir:

```bash
data/AMi/<title>/ScreenShots/screenShotOnError_yyyy-mm-dd_HH-MM-SS.png
```


